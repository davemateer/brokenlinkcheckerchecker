﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace dnet.Pages
{
    public class SlowloadingpageModel : PageModel
    {
        private readonly ILogger<PrivacyModel> _logger;

        public SlowloadingpageModel(ILogger<PrivacyModel> logger)
        {
            _logger = logger;
        }

        public async Task OnGetAsync()
        {
            await Task.Delay(5000);
        }
    }
}
